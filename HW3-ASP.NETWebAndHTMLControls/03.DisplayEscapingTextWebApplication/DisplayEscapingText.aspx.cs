﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _03.DisplayEscapingTextWebApplication
{
    public partial class DisplayEscapingText : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonText_Click(object sender, EventArgs e)
        {
            this.TextBoxText.Text = TextBoxFirstNumber.Text;  //TextBox alone escaping
            this.LabelText.Text = Server.HtmlEncode(TextBoxFirstNumber.Text);
            this.LiteralEnteredText.Text = TextBoxFirstNumber.Text;
        }
    }
}