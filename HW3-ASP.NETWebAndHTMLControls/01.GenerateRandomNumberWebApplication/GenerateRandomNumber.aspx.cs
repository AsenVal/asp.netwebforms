﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _01.GenerateRandomNumberWebApplication
{
    public partial class GenerateRandomNumber : System.Web.UI.Page
    {
        Random rand = new Random();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonGenerate_Click(object sender, EventArgs e)
        {
            int firstNumber;
            int secondNumber;
            bool isFirstNumber = int.TryParse(this.TextBoxFirstNumber.Text, out firstNumber);
            bool isSecondNumber = int.TryParse(this.TextBoxSecondNumber.Text, out secondNumber);
            if (isFirstNumber && isSecondNumber)
            {
                this.resultNumber.Text = Convert.ToString(rand.Next(firstNumber, secondNumber));
            }
            else
            {
                this.resultNumber.Text = "error";
            }
        }
    }
}