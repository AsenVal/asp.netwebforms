﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateRandomNumber.aspx.cs" Inherits="_01.GenerateRandomNumberWebApplication.GenerateRandomNumber" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        span{
            padding:5px;
            width:100px;
            display: inline-block;
        }
        #panel{
            width:280px;
            border:1px solid blue;
            border-radius:5px;
            padding:5px;
        }
    </style>
</head>
<body>
    <form id="GenerateRandomNumber" runat="server">
    <h2>Generate random number in range.</h2>
        <div id="container">
            <div>
                <span id="LabelFirstNumber">First number</span>
                <input type="text" runat="server" id="TextBoxFirstNumber"/>
            </div>
            <div>
                <span id="LabelSecondNumber">Second number</span>
                <input type="text" runat="server" id="TextBoxSecondNumber" />
            </div>
            <input value="Generate" type="button" width="80px" runat="server" id="ButtonGenerate" onserverclick="ButtonGenerate_Click" />
        </div>
        <div>
            <span id"LabelSum">Random number is:</span>
            <h1 runat="server" id="resultNumber"></h1>
        </div>
    </form>
</body>
</html>
