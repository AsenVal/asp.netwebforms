﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _01.CarSearchinFormWebApplication
{
    public class Producer
    {
        public string Name { get; set; }

        public int Id { get; set; }
        public List<string> Models { get; set; }
        public Producer()
        {
        }
    }
}