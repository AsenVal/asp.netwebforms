﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _01.PrintUserNameWebApplication
{
    public partial class PrintUserName : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonDisplayName_Click(object sender, EventArgs e)
        {
            this.DisplayName.InnerText = "Hello " + this.TextBoxEnterYourName.Text + "!";
        }
    }
}