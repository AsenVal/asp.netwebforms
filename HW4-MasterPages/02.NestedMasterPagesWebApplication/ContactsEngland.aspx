﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Contacts.master" AutoEventWireup="true" CodeBehind="ContactsEngland.aspx.cs" Inherits="_02.NestedMasterPagesWebApplication.Bulgaria" %>

<asp:Content ID="ContentContacts" ContentPlaceHolderID="ContentPlaceHolderContacts" 
runat="server">
    <p>Our address in England is:</p>
    <ul>
        <li>Country: England</li>
        <li>Town: London</li>
        <li>Tel: +4928445565815</li>
    </ul>
</asp:Content>