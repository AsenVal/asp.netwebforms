﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CarSearchinForm.aspx.cs" Inherits="_01.CarSearchinFormWebApplication.CarSearchinForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="carSearchingForm" runat="server">
        <h1>Searching cars.</h1>
        <asp:DropDownList ID="DropDownProducers" runat="server" AutoPostBack="true"
            DataTextField="Name" DataValueField="Id"
            OnSelectedIndexChanged="DropDownProducers_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:DropDownList ID="DropDownListModels" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        <asp:CheckBoxList ID="CheckBoxListExtas" runat="server" DataTextField ="Name" DataValueField="Name">
        </asp:CheckBoxList>
        <asp:Button ID="ButtonSubmit" Text="Submit" runat="server" OnClick="ButtonSubmit_Click" />
        <asp:Literal ID="submitResult" Text="" runat="server" />
    </form>
</body>
</html>
