﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeDetails.aspx.cs" Inherits="_02.EmployeesWebApplicationUsingDetailView.EmployeeDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="EmployeesDetailsForm" runat="server">
        <asp:DetailsView runat="server" ID="EmployeeDetailsView" AutoGenerateRows="true"/> 
        <asp:HyperLink runat="server" NavigateUrl="~/Employees.aspx" Text="Back" ></asp:HyperLink>
    </form>
</body>
</html>
