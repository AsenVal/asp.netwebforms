﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _02.SayHelloAndDisplayAssemblyPathWebApplication
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.LabelHello.Text = "Hello from ASP.Net C# code.";
            this.LabelAssemblyPath.Text = Assembly.GetExecutingAssembly().Location;
        }
    }
}