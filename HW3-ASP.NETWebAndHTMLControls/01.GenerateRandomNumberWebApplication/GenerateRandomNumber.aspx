﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateRandomNumber.aspx.cs" Inherits="_01.GenerateRandomNumberWebApplication.GenerateRandomNumber" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        span{
            padding:5px;
            width:100px;
            display: inline-block;
        }
        #panel{
            width:280px;
            border:1px solid blue;
            border-radius:5px;
            padding:5px;
        }
    </style>
</head>
<body>
    <form id="GenerateRandomNumber" runat="server">
    <h2>Generate random number in range.</h2>
        <asp:Panel runat="server" ID="container">
            <div>
                <asp:Label Text="First number" runat="server" ID="LabelFirstNumber" />
                <asp:TextBox runat="server" ID="TextBoxFirstNumber" />
            </div>
            <div>
                <asp:Label Text="Second number" runat="server" ID="LabelSecondNumber" />
                <asp:TextBox runat="server" ID="TextBoxSecondNumber" />
            </div>
            <asp:Button Width="80px" Text="Generate" runat="server" ID="ButtonGenerate" OnClick="ButtonGenerate_Click" />
        </asp:Panel>
        <div>
            <asp:Label Text="Random number is:" runat="server" ID="LabelSum" />
            <asp:Label Text="" runat="server" ID="resultNumber" Font-Size="XX-Large" />
        </div>
    </form>
</body>
</html>
