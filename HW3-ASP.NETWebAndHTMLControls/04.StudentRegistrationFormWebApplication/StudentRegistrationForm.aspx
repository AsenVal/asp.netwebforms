﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentRegistrationForm.aspx.cs" Inherits="_04.StudentRegistrationFormWebApplication.StudentRegistrationForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        span{
            padding:5px;
            width:100px;
            display: inline-block;
        }
        #panel{
            width:280px;
            border:1px solid blue;
            border-radius:5px;
            padding:5px;
        }
    </style>
</head>
<body>
    <form id="registrationForm" runat="server">
        <h2>Student registration form.</h2>
        <asp:Panel runat="server" ID="container">
            <div>
                <asp:Label Text="First name" runat="server" ID="LabelFirstName" />
                <asp:TextBox runat="server" ID="TextBoxFirstName" />
            </div>
            <div>
                <asp:Label Text="Second name" runat="server" ID="LabelSecondName" />
                <asp:TextBox runat="server" ID="TextBoxSecondName" />
            </div>
            <div>
                <asp:Label Text="Faculty number" runat="server" ID="LabelFacultyNumber" />
                <asp:TextBox runat="server" ID="TextBoxFacultyNumber" />
            </div>
            <div>
                <asp:Label Text="University" runat="server" ID="LabelUniversity" />
                <asp:DropDownList runat="server" ID="DropDownListUniversity" AutoPostBack="False">
                    <asp:ListItem Value="1">UNSS</asp:ListItem>
                    <asp:ListItem Value="2">UASG</asp:ListItem>
                    <asp:ListItem Value="3">Telerik Academy</asp:ListItem>
                </asp:DropDownList>
            </div>
			<div>
                <asp:Label Text="Specialty" runat="server" ID="LabelSpecialty" />
                <asp:DropDownList runat="server" ID="DropDownListSpecialty" AutoPostBack="False">
                    <asp:ListItem Value="1">Programing</asp:ListItem>
                    <asp:ListItem Value="2">Engineering</asp:ListItem>
                    <asp:ListItem Value="3">Finance</asp:ListItem>
                </asp:DropDownList>
            </div>
			<asp:Label Text="Courses" runat="server" ID="LabelCourses"/>
			<asp:ListBox ID="ListBoxCourses" runat="server" AutoPostBack="False" Height="56px" SelectionMode="Multiple">
				<asp:ListItem Value="1">CSS</asp:ListItem>
				<asp:ListItem Value="2">HTML</asp:ListItem>
				<asp:ListItem Value="3">Javascript</asp:ListItem>
				<asp:ListItem Value="4">C#</asp:ListItem>
				<asp:ListItem Value="5">OOP</asp:ListItem>
				<asp:ListItem Value="6">ASP.Net</asp:ListItem>
				<asp:ListItem Value="7">Database</asp:ListItem>
			</asp:ListBox>
            <asp:Button Width="80px" Text="Submit" runat="server" ID="ButtonSubmit" OnClick="ButtonSubmit_Click" />
        </asp:Panel >
        <asp:Panel runat="server" ID="resultContainer" Visible="false">
		<h1>Your submited information is:</h1>
			<span>First name: </span>
            <asp:Label Text="" runat="server" ID="LabelFirstNameResult" />
            <br />
			<span>Second name: </span>
            <asp:Label Text="" runat="server" ID="LabelSecondNameResult" />
            <br />
			<span>Faculty number: </span>
            <asp:Label Text="" runat="server" ID="LabelFacultyNumberResult" />
            <br />
			<span>University: </span>
            <asp:Label Text="" runat="server" ID="LabelUniversityResult" />
            <br />
			<span>Specialty: </span>
            <asp:Label Text="" runat="server" ID="LabelSpecialtyResult" />
			<br />
			<span>Courses: </span>
			<asp:BulletedList ID="ListBoxCoursesResult" runat="server" ></asp:BulletedList>
        </asp:Panel>
    </form>
</body>
</html>
