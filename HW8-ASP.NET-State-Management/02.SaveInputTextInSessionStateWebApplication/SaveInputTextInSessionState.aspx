﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SaveInputTextInSessionState.aspx.cs" Inherits="_02.SaveInputTextInSessionStateWebApplication.SaveInputTextInSessionState" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="mainForm" runat="server">
        <asp:TextBox ID="TextBoxInput" runat="server"></asp:TextBox>
        <asp:Button ID="ButtonSaveToSessionStage" OnClick="ButtonSaveToSessionStage_Click" runat="server" Text="Submit" />
        <asp:Label ID="LabelOutput" runat="server"></asp:Label>
    </form>
</body>
</html>
