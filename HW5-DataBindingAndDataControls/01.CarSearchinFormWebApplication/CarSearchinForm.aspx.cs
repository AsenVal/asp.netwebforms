﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _01.CarSearchinFormWebApplication
{
    public partial class CarSearchinForm : System.Web.UI.Page
    {
        List<Producer> producers = new List<Producer>() {
            new Producer() { Id = 1, Name="All", Models = new List<string>(){ "All" } },
            new Producer() { Id = 2, Name="BMW", Models = new List<string>(){ "All", "114", "116", "118", "1500", "1800", "2000", "M3", "M5", "M6" } },
            new Producer() { Id = 3, Name="Audi", Models = new List<string>(){ "All", "80", "90", "100", "A4", "A6", "A8" } },
            new Producer() { Id = 4, Name="Mercedes", Models = new List<string>(){ "All", "A150", "A200", "C200", "C220", "C240", "CLC", "S400", "S500", "SLS" } },
            new Producer() { Id = 5, Name="Opel", Models = new List<string>(){ "All", "Astra", "Corsa", "Kadett", "Omega", "Vectra", "Zafira" } },
            new Producer() { Id = 6, Name="VW", Models = new List<string>(){ "All", "Golf", "Jetta", "Lupo", "Passat", "Polo" } },
        };

        List<Extra> extras = new List<Extra>() { 
            new Extra() { Name = "GPS system" },
            new Extra() { Name = "Automat Climate Control" },
            new Extra() { Name = "Auxiliary heating" },
            new Extra() { Name = "Electric heated seats" },
            new Extra() { Name = "ABS" },
            new Extra() { Name = "Immobilizer" },
            new Extra() { Name = "Xenon headlights" },
            new Extra() { Name = "Parking sensors" },
            new Extra() { Name = "4(5) doors" },
            new Extra() { Name = "USB, audio\\video, IN\\AUX" },
        };
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.DropDownProducers.DataSource = producers;
                this.CheckBoxListExtas.DataSource = extras;
                int id = 0;
                if (int.TryParse(this.DropDownProducers.SelectedValue, out id))
                {
                    this.DropDownListModels.DataSource = producers[id - 1].Models;
                }
                else
                {
                    this.DropDownListModels.DataSource = producers[0].Models;
                }

                Page.DataBind();
            }
        }

        protected void DropDownProducers_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = int.Parse(this.DropDownProducers.SelectedValue);
            this.DropDownListModels.DataSource = producers[id - 1].Models;
            this.DropDownListModels.DataBind();
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine("<h3> Your searching car parametars are:</h3>");
            result.AppendLine("<div>Car Producer: " + Server.HtmlEncode(this.DropDownProducers.SelectedItem.Text) + "</div>");
            result.AppendLine("<div>Car Model: " + Server.HtmlEncode(this.DropDownListModels.SelectedValue) + "</div>");
            result.AppendLine("<ul>");
            for (int i = 0; i < CheckBoxListExtas.Items.Count; i++)
            {
                if (CheckBoxListExtas.Items[i].Selected)
                {
                    result.AppendLine("<li>" + Server.HtmlEncode(this.CheckBoxListExtas.Items[i].Text) + "</li>");
                }
            }

            result.AppendLine("</ul>");
            this.submitResult.Text = result.ToString();
        }

    }
}