﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _02.EmployeesWebApplicationUsingDetailView
{
    public partial class EmployeeDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["Id"] == null)
            {
                Response.Redirect("Employees.aspx");
            }
            else
            {
                var context = new NorthwindEntities();
                using (context)
                {
                    int id = int.Parse(Request.Params["Id"]);
                    var employee = context.Employees.FirstOrDefault(em => em.EmployeeID == id);
                    this.EmployeeDetailsView.DataSource = new List<Employee>() { employee };
                    Page.DataBind();
                }
            }
        }
    }
}