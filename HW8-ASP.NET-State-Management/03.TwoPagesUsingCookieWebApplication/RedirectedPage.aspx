﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RedirectedPage.aspx.cs" Inherits="_03.TwoPagesUsingCookieWebApplication.RediretedPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="mainForm" runat="server">
        <asp:Button runat="server" ID="buttonBack" Text="Go Back" OnClick="buttonBack_Click"/>
        <br />
        <asp:Literal runat="server" ID="Literal"></asp:Literal>
    </form>
</body>
</html>
