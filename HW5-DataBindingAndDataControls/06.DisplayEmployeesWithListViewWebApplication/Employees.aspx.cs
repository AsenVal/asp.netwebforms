﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _06.DisplayEmployeesWithListViewWebApplication
{
    public partial class Employees : System.Web.UI.Page
    {
        public IList<Employee> GetEmployees()
        {
            var context = new NorthwindEntities();
            using (context)
            {
                var employees = context.Employees.ToList();

                return employees;

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ListViewEmployees.DataSource = this.GetEmployees();

            Page.DataBind();

        }
    }
}