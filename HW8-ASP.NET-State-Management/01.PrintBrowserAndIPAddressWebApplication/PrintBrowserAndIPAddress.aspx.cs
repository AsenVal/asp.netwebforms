﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _01.PrintBrowserAndIPAddressWebApplication
{
    public partial class PrintBrowserAndIPAddress : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.LiteralOutput.Text = Request.UserAgent + "<br/>";
            //this.LiteralOutput.Text += Request.UserHostAddress.ToString().Trim();
            this.LiteralOutput.Text += Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];  
        }
    }
}