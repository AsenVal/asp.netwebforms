﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="_06.DisplayEmployeesWithListViewWebApplication.Employees" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Style.css" rel="stylesheet" />
</head>
<body>
    <form id="EmployeesForm" runat="server">
        <asp:ListView ID="ListViewEmployees" runat="server" ItemType="_06.DisplayEmployeesWithListViewWebApplication.Employee">
            <LayoutTemplate>
                <h3>Employees</h3>
                <asp:PlaceHolder runat="server" ID="itemPlaceholder"></asp:PlaceHolder>
            </LayoutTemplate>

            <ItemSeparatorTemplate>
                <hr />
            </ItemSeparatorTemplate>

            <ItemTemplate>    
                <span class="product-description">
                    <h4><%#: Item.TitleOfCourtesy + " " + Item.FirstName + " " + Item.LastName %></h4>
                    <div>BirthDate: <%#: Item.BirthDate %></div>
                    <div>Address: <%#: Item.Address %></div>
                    <div>Hire date: <%#: Item.HireDate%></div>
                    <div>Home phone: <%#: Item.HomePhone%></div>
                    <div>Notes: <%#: Item.Notes%></div>  
                    <div>Region: <%#: Item.Region%></div>
                    <div>Country: <%#: Item.Country%></div>
                    <div>City: <%#: Item.City%></div>
                    <div>PostalCode: <%#: Item.PostalCode%></div>
                    <div>Title: <%#: Item.Title%></div>
            </ItemTemplate>
        </asp:ListView>

        <asp:DataPager ID="DataPagerEmployees" runat="server"
            PagedControlID="ListViewEmployees" PageSize="3"
            QueryStringField="page">
            <Fields>
                <asp:NextPreviousPagerField ShowFirstPageButton="true"
                    ShowNextPageButton="false" ShowPreviousPageButton="false" />
                <asp:NumericPagerField />
                <asp:NextPreviousPagerField ShowLastPageButton="true"
                    ShowNextPageButton="false" ShowPreviousPageButton="false" />
            </Fields>
        </asp:DataPager>
    </form>
</body>
</html>
