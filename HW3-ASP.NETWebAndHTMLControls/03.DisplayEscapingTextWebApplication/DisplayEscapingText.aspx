﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisplayEscapingText.aspx.cs" Inherits="_03.DisplayEscapingTextWebApplication.DisplayEscapingText" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="EscapingText" runat="server">
    <h2>Escaping text example.</h2>
        <asp:Panel runat="server" ID="container">
            <div>
                <asp:Label Text="Enter text" runat="server" ID="LabelFirstNumber" />
                <asp:TextBox runat="server" ID="TextBoxFirstNumber" />
            </div>
            <asp:Button Width="80px" Text="Generate" runat="server" ID="ButtonGenerate" OnClick="ButtonText_Click" />
        </asp:Panel>
        <div>
            <asp:Label Text="" runat="server" ID="LabelText" />
            <br />
            <asp:Literal ID="LiteralEnteredText" runat="server" Mode="Encode" />
        </div>
        <div>
            <asp:TextBox runat="server" ID="TextBoxText" />
        </div>
    </form>
</body>
</html>
