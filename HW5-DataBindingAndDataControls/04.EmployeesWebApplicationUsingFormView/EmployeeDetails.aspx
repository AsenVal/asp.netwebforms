﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeDetails.aspx.cs" Inherits="_04.EmployeesWebApplicationUsingFormView.EmployeeDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="EmployeesDetailsForm" runat="server">
        <asp:FormView runat="server" ID="EmployeeDetailsView" ItemType="_04.EmployeesWebApplicationUsingFormView.Employee">
            <ItemTemplate>  
                <h3><%#: Item.TitleOfCourtesy + " " + Item.FirstName + " " + Item.LastName %></h3>
                <table border="0">
                    <tr>
                        <td>BirthDate:</td>
                        <td><%#: Item.BirthDate%></td>
                    </tr>
                    <tr>
                        <td>Hire date:</td>
                        <td><%#: Item.HireDate%></td>
                    </tr>
                    <tr>
                        <td>Home phone:</td>
                        <td><%#: Item.HomePhone%></td>
                    </tr>
                    <tr>
                        <td>Notes:</td>
                        <td><%#: Item.Notes%></td>
                    </tr>                 
                    <tr>
                        <td>Region:</td>
                        <td><%#: Item.Region%></td>
                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td><%#: Item.Country%></td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td><%#: Item.City%></td>
                    </tr>
                    <tr>
                        <td>PostalCode:</td>
                        <td><%#: Item.PostalCode%></td>
                    </tr>
                    <tr>
                        <td>Title:</td>
                        <td><%#: Item.Title%></td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:FormView>
        <asp:HyperLink runat="server" NavigateUrl="~/Employees.aspx" Text="Back" ></asp:HyperLink>
    </form>
</body>
</html>
