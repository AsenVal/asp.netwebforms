﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _03.TwoPagesUsingCookieWebApplication
{
    public partial class RediretedPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            foreach (string item in Request.Cookies)
            {
                Literal.Text += Request.Cookies[item].Value + "<br/>";
            }
        }
        protected void buttonBack_Click(object sender, EventArgs e)
        {
            HttpCookie cookie = new HttpCookie(new Random().Next().ToString(), "Cookie Set from RedirectedPage.aspx");
            cookie.Expires = DateTime.Now.AddMinutes(1);
            Response.Cookies.Add(cookie);
            Response.Redirect("TwoPagesUsingCookies.aspx");
        }
    }
}