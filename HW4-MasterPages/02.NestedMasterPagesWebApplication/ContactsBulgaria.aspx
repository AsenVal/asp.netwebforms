﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Contacts.master" AutoEventWireup="true" CodeBehind="ContactsBulgaria.aspx.cs" Inherits="_02.NestedMasterPagesWebApplication.Bulgaria" %>

<asp:Content ID="ContentContacts" ContentPlaceHolderID="ContentPlaceHolderContacts" 
runat="server">
    <p>Our address in Bulgaria is:</p>
    <ul>
        <li>Country: Bulgaria</li>
        <li>Town: Sofia</li>
        <li>Tel: +359284565815</li>
    </ul>
</asp:Content>