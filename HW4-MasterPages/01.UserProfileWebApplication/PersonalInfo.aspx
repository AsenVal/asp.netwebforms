﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
    CodeBehind="PersonalInfo.aspx.cs" Inherits="_01.UserProfileWebApplication.PersonalInfo" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolderHeader" runat="server">
    <meta name="site" content="Try master pages"/>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <h1>Personal info</h1>
    <div> 
        <span class="info">First name: </span>
        <span class="result">Doncho </span>
    </div>
    <div> 
        <span class="info">Last name: </span>
        <span class="result">Minkov </span>
    </div>
    <div> 
        <span class="info">Age: </span>
        <span class="result">26 </span>
    </div>
</asp:Content>
