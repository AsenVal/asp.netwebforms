﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _03.TwoPagesUsingCookieWebApplication
{
    public partial class TwoPagesUsingCookies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonRedirect_Click(object sender, EventArgs e)
        {
            HttpCookie cookie = new HttpCookie(new Random().Next().ToString(), "Cookie Set from TwoPagesUsingCookies.aspx");
            cookie.Expires = DateTime.Now.AddMinutes(1);
            Response.Cookies.Add(cookie);
            Response.Redirect("RedirectedPage.aspx");
        }
    }
}