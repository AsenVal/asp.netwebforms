﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _04.StudentRegistrationFormWebApplication
{
    public partial class StudentRegistrationForm : System.Web.UI.Page
    {        	
        protected void Page_Load(object sender, EventArgs e)
        {

        }
		
		protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
			this.LabelFirstNameResult.Text = Server.HtmlEncode(TextBoxFirstName.Text);
			this.LabelSecondNameResult.Text = Server.HtmlEncode(TextBoxSecondName.Text);
			this.LabelFacultyNumberResult.Text = Server.HtmlEncode(TextBoxFacultyNumber.Text);
			this.LabelUniversityResult.Text = DropDownListUniversity.SelectedItem.Text;
			this.LabelSpecialtyResult.Text = DropDownListSpecialty.SelectedItem.Text;
			
			// adding selected items to bulletlist
			  
			var selectedItems = ListBoxCourses.Items.GetSelectedItems();
            this.ListBoxCoursesResult.Items.Clear();
			foreach(var item in selectedItems)
			{
                ListItem li = new ListItem();
				li.Text = item.ToString();  
				this.ListBoxCoursesResult.Items.Add(li);  
			}
            this.resultContainer.Visible = true;
        }
    }

    public static class ListItemExtensions
    {
        public static IEnumerable<ListItem> GetSelectedItems(
               this ListItemCollection items)
        {
            return items.OfType<ListItem>().Where(item => item.Selected);
        }
    }
}