﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _02.SaveInputTextInSessionStateWebApplication
{
    public partial class SaveInputTextInSessionState : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TextInputList"] == null)
            {
                Session.Add("TextInputList", new List<string>());
                //Session["TextInputList"] = new List<string>();
            }

        }

        protected void ButtonSaveToSessionStage_Click(object sender, EventArgs e)
        {
            string input = this.TextBoxInput.Text;
            if (input != "")
            {
                var textInput = (Session["TextInputList"] as List<string>);
                textInput.Add(input);
                LabelOutput.Text = "";
                foreach (var item in textInput)
                {
                    LabelOutput.Text += "<br/>" + Server.HtmlEncode(item);
                }

                TextBoxInput.Text = "";
            }
        }
    }
}