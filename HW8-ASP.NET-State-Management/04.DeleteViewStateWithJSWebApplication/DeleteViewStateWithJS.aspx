﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteViewStateWithJS.aspx.cs" Inherits="_04.DeleteViewStateWithJSWebApplication.DeleteViewStateWithJS" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-2.0.3.js"></script>
</head>
<body>
    <form id="mainForm" runat="server">
        <asp:TextBox ID="TextBoxInput" runat="server"></asp:TextBox>
        <asp:Button ID="ButtonSaveToSessionStage" OnClick="ButtonSaveToSessionStage_Click" runat="server" Text="Submit" />        
        <button id="delete-viewstate" >Delete ViewState</button>
        <asp:Label ID="LabelOutput" runat="server"></asp:Label>
    </form>
    <script>
        $(document).ready(
        $("#mainForm").on("click", "#delete-viewstate", function () {

            $("#__VIEWSTATE").val("");
            document.location.reload();
        })
        );
    </script>
</body>
</html>
