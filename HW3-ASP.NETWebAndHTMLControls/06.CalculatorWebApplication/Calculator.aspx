﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculator.aspx.cs" Inherits="_06.CalculatorWebApplication.Calculator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        form {
            width:280px;
            display:block;
            margin: auto;
        }
        
        span{
            padding:5px;
            width:100px;
            display: inline-block;
        }
        .container{
            border:1px solid blue;
            border-radius:3px;
            padding:5px 20px;
            margin:5px auto;
            width: 215px;
            display:inline-block;
        }
		.button{
			width:50px;
            height:30px;
			display: inline;
		}
        input{
            display:block;
            margin: auto;
            width:100px;
        }
    </style>
</head>
<body>
    <form id="Calculatorform" runat="server">
		<asp:Panel runat="server" ID="containerInput" CssClass="container">
			<asp:TextBox runat="server" ID="TextBoxInput" />
		</asp:Panel>
		<asp:Panel runat="server" ID="containerButtons" CssClass="container">
			<div>
				<asp:Button Text="1" runat="server" ID="Button1" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="2" runat="server" ID="Button2" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="3" runat="server" ID="Button3" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="+" runat="server" ID="ButtonPlus" CssClass="button" OnClick="ButtonPlus_Click" />
			</div>          
			<div>           
				<asp:Button Text="4" runat="server" ID="Button4" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="5" runat="server" ID="Button5" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="6" runat="server" ID="Button6" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="-" runat="server" ID="ButtonMinus" CssClass="button" OnClick="ButtonMinus_Click" />
			</div>
			<div>
				<asp:Button Text="7" runat="server" ID="Button7" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="8" runat="server" ID="Button8" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="9" runat="server" ID="Button9" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="*" runat="server" ID="ButtonMultiple" CssClass="button" OnClick="ButtonMultiple_Click" />
			</div>
			<div>
				<asp:Button Text="Clear" runat="server" ID="ButtonClear" CssClass="button" OnClick="ButtonClear_Click" />
				<asp:Button Text="0" runat="server" ID="Button0" CssClass="button" OnClick="ButtonNumber_Click" />
				<asp:Button Text="/" runat="server" ID="ButtonDivide" CssClass="button" OnClick="ButtonDivide_Click" />
				<asp:Button Text="" runat="server" ID="ButtonSqrt" CssClass="button" OnClick="ButtonSqrt_Click" />
			</div>
		</asp:Panel>
		<asp:Panel runat="server" ID="containerResult" CssClass="container">
			<asp:Button Text="=" runat="server" ID="ButtonResult" OnClick="ButtonResult_Click" />
		</asp:Panel>
		<asp:Label Text="0" runat="server" ID="LabelCurrentNumber" Visible="false"/>
		<asp:Label Text="" runat="server" ID="LabelOperation" Visible="false"/>
		<asp:Label Text="false" runat="server" ID="LabelNewNumber" Visible="false"/>
		<asp:Label Text="false" runat="server" ID="LabelCalculate" Visible="false"/>
    </form>
</body>
</html>
