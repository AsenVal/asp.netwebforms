using Owin;

namespace _02.SayHelloAndDisplayAssemblyPathWebApplication
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
