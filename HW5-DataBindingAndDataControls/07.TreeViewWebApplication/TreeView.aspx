﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TreeView.aspx.cs" Inherits="_07.TreeViewWebApplication.TreeView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="TreeViewForm" runat="server">
        <asp:TreeView ID="TreeViewXML" runat="server" DataSourceID="soruce"
            OnTreeNodeCollapsed="TreeViewXML_TreeNodeCollapsed" SelectedNodeStyle-ForeColor="Green"
            OnSelectedNodeChanged="TreeViewXML_SelectedNodeChanged">
            <DataBindings>
                <asp:TreeNodeBinding TargetField="#InnerText" TextField="#Name" />
                <asp:TreeNodeBinding DataMember="genre" TextField="name" />
                <asp:TreeNodeBinding DataMember="book" TextField="ISBN" />
                <asp:TreeNodeBinding DataMember="userComment" TargetField="#InnerText" TextField="rating" />
                <asp:TreeNodeBinding DataMember="comments" TextField="#Name" />
            </DataBindings>

            <SelectedNodeStyle ForeColor="Green"></SelectedNodeStyle>
        </asp:TreeView>
        <asp:XmlDataSource ID="soruce" runat="server" DataFile="~/books.xml"></asp:XmlDataSource>
        <asp:Label ID="innerXml" runat="server"></asp:Label>
    </form>
</body>
</html>
