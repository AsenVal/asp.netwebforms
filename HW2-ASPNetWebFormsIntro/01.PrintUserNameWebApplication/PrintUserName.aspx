﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintUserName.aspx.cs" Inherits="_01.PrintUserNameWebApplication.PrintUserName" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="printUserName" runat="server">
        <asp:Label Text="Enter your name" runat="server" ID="LabelEnterYourName" />
        <asp:TextBox Text="" runat="server" ID="TextBoxEnterYourName" />
        <asp:Button Text="Click me!" runat="server" ID="ButtonDisplayName" OnClick="ButtonDisplayName_Click" />
        <h2 runat="server" id="DisplayName"></h2>
    </form>
</body>
</html>
