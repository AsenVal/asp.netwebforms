﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="_05.DisplayEmployeesWithRepeaterWebApplication.Employees" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="EmployeesForm" runat="server">
        <asp:Repeater ID="RepeaterEmployees" runat="server" ItemType="_05.DisplayEmployeesWithRepeaterWebApplication.Employee">
            <HeaderTemplate>
                <table border="0">
                    <tr>
                        <td>Title of courtesy</td>
                        <td>First name:</td>
                        <td>Last name:</td>
                        <td>Bithday:</td>
                        <td>Address:</td>
                        <td>Hire date:</td>
                        <td>Home phone:</td>
                        <td>Notes:</td>
                        <td>Region:</td>
                        <td>Country:</td>
                        <td>City:</td>
                        <td>PostalCode:</td>
                        <td>Title:</td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>                
                    <tr>
                        <td><%#: Item.TitleOfCourtesy %></td>
                        <td><%#: Item.FirstName %></td>
                        <td><%#: Item.LastName %></td>
                        <td><%#: Item.BirthDate %></td>
                        <td><%#: Item.Address %></td>
                        <td><%#: Item.HireDate%></td>
                        <td><%#: Item.HomePhone%></td>
                        <td><%#: Item.Notes%></td>  
                        <td><%#: Item.Region%></td>
                        <td><%#: Item.Country%></td>
                        <td><%#: Item.City%></td>
                        <td><%#: Item.PostalCode%></td>
                        <td><%#: Item.Title%></td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>        
                </table>
            </FooterTemplate>    
        </asp:Repeater>
    </form>
</body>
</html>
