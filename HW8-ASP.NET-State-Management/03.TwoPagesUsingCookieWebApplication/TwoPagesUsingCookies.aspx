﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TwoPagesUsingCookies.aspx.cs" Inherits="_03.TwoPagesUsingCookieWebApplication.TwoPagesUsingCookies" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="MainForm" runat="server">
        <asp:Button runat="server" ID="ButtonRedirect" Text="Redirect" OnClick="ButtonRedirect_Click"/>
    </form>
</body>
</html>
