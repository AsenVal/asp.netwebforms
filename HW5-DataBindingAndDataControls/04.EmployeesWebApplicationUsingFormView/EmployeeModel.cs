﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _04.EmployeesWebApplicationUsingFormView
{
    public class EmployeeModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }
}