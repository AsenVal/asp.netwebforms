﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="_04.EmployeesWebApplicationUsingFormView.Employees" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="employeesForm" runat="server">
        <asp:GridView ID="GridViewEmployees" runat="server"
            AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" 
            CellPadding="2" ForeColor="Black" GridLines="None">
            <Columns>
                <asp:HyperLinkField DataTextField="FullName" HeaderText="Employee Name"
                DataNavigateUrlFields="Id" DataNavigateUrlFormatString="EmployeeDetails.aspx?id={0}"/>
            </Columns>

        </asp:GridView>
    </form>
</body>
</html>
